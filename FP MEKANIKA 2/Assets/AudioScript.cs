﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{
    public AudioSource audio;
    private static AudioScript _instance;

    static AudioSource sfxSource;
    private static AudioClip click, popup, click2, inst, clickYes;
    // Start is called before the first frame update

    public static AudioScript instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.FindObjectOfType<AudioScript>();
                DontDestroyOnLoad(_instance.gameObject);

            }
            return _instance;
        }
    }
    void Start()
    {
        click = Resources.Load<AudioClip>("click");
        click2 = Resources.Load<AudioClip>("click2");
        popup = Resources.Load<AudioClip>("popup");
        inst = Resources.Load<AudioClip>("hit");
        clickYes = Resources.Load<AudioClip>("clickYes");
    }

    void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            if (this != _instance)
                Destroy(this.gameObject);
        }
    
    }

    public static void PlaySFX(string audio)
    {
        switch (audio)
        {
            case "click":
                sfxSource.PlayOneShot(click);
                break;
            case "click2":
                sfxSource.PlayOneShot(click2);
                break;
            case "popup":
                sfxSource.PlayOneShot(popup);
                break;
            case "inst":
                sfxSource.PlayOneShot(inst);
                break;
            case "clickYes":
                sfxSource.PlayOneShot(clickYes);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
