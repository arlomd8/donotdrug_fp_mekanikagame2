﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class GameControl : MonoBehaviour {

    private static GameObject whoWinsTextShadow, player1MoveText, player2MoveText, playerPopUp, player1bg, player2bg, pausePopUp;

    private static GameObject player1, player2;

    public GameObject donut, drug;
    //GameObject donutObject;
    //GameObject drugObject;

    public TextMeshProUGUI priceTag, returnValTag;


    public static int diceSideThrown = 0;
    public int player1StartWaypoint = 0;
    public int player2StartWaypoint = 0;

    public static bool gameOver = false;
    public static bool pauseGame = false;
    public Dice dice;
    public PropertyWaypoint property;

    private static int waypointStop;
    private int waypointJail = 6;
    private int waypointTax = 12;
    private int waypointHouse = 18;
    private int price;
    private int returnVal;
    private bool pop1, pop2;
    private int priceStop;

    private static bool paid;

    void Start () {

        whoWinsTextShadow = GameObject.Find("WhoWinsText");
        player1MoveText = GameObject.Find("Player1MoveText");
        player2MoveText = GameObject.Find("Player2MoveText");
        playerPopUp = GameObject.Find("PopUpWindow");
        pausePopUp = GameObject.Find("PopUpPause");
        donut = GameObject.Find("DonutMark");
        drug = GameObject.Find("DrugMark");
        player1bg = GameObject.Find("DonutBG");
        player2bg = GameObject.Find("DrugBG");

        player1 = GameObject.Find("Player1");
        player2 = GameObject.Find("Player2");

        player1.GetComponent<FollowThePath>().moveAllowed = true;
        player2.GetComponent<FollowThePath>().moveAllowed = false;
        
        whoWinsTextShadow.gameObject.SetActive(false);
        player1MoveText.gameObject.SetActive(true);
        player2MoveText.gameObject.SetActive(false);
        playerPopUp.gameObject.SetActive(false);
        player1bg.gameObject.SetActive(true);
        player2bg.gameObject.SetActive(false);
        pausePopUp.gameObject.SetActive(false);

        Debug.Log(waypointStop);

        //waypointStop = player2.GetComponent<FollowThePath>().waypointIndex;
        //waypointStop = player1.GetComponent<FollowThePath>().waypointIndex;

        //priceStop = property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>();
    }

 
    void Update()
    {
        if (!player1.GetComponent<FollowThePath>().moveAllowed && dice.whosTurn == -1)
        {
            waypointStop = player1.GetComponent<FollowThePath>().waypointIndex;

            //print(waypointStop);
            priceStop = property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().buyPrice;
            if (!property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().bought && !pauseGame)
            {
                if (player1.GetComponent<Money>().money >= property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().buyPrice)
                {
                    pop1 = true;
                    InvestPopUp(waypointStop);
                }
                else
                {
                    NoInvest();
                }
            }
            else
            {
                if (property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().player == 2 && !paid)
                {
                    player2.GetComponent<Money>().money -= property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().lostToll;
                    player1.GetComponent<Money>().money += property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().lostToll;
                    paid = true;
                }

                if (property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().player == 1 && !paid)
                {

                    player1.GetComponent<Money>().money += property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().returVal;
                    paid = true;
                }
                if (player1.GetComponent<FollowThePath>().jailed && !paid)
                {
                    Debug.Log("PENJARA");
                    player1.GetComponent<FollowThePath>().jailed = false;
                    player1.GetComponent<Money>().money += property.propertyWaypoint[waypointJail].GetComponent<PropertyPrice>().returVal;
                    paid = true;
                    player1MoveText.gameObject.SetActive(false);
                    player1bg.gameObject.SetActive(false);
                    player2MoveText.gameObject.SetActive(true);
                    player2bg.gameObject.SetActive(true);
                }
                if (player1.GetComponent<FollowThePath>().taxes && !paid)
                {
                    Debug.Log("PAJAK");
                    player1.GetComponent<FollowThePath>().taxes = false;
                    player1.GetComponent<Money>().money -= property.propertyWaypoint[waypointTax].GetComponent<PropertyPrice>().lostToll;
                    paid = true;
                    player1MoveText.gameObject.SetActive(false);
                    player1bg.gameObject.SetActive(false);
                    player2MoveText.gameObject.SetActive(true);
                    player2bg.gameObject.SetActive(true);
                }
                if (player1.GetComponent<FollowThePath>().warehouse && !paid)
                {
                    Debug.Log("WAREHOUSE");
                    player1.GetComponent<FollowThePath>().warehouse = false;
                    player1.GetComponent<Money>().life -= 1;
                    paid = true;
                    player1MoveText.gameObject.SetActive(false);
                    player1bg.gameObject.SetActive(false);
                    player2MoveText.gameObject.SetActive(true);
                    player2bg.gameObject.SetActive(true);
                }

                player1StartWaypoint = waypointStop;




            }
        }


        if (!player2.GetComponent<FollowThePath>().moveAllowed && dice.whosTurn == 1)
        {
            waypointStop = player2.GetComponent<FollowThePath>().waypointIndex;
            //print(waypointStop);
            priceStop = property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().buyPrice;
            if (!property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().bought && !pauseGame)
            {
                if (player2.GetComponent<Money>().money >= property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().buyPrice)
                {
                    pop2 = true;
                    InvestPopUp(waypointStop);
                }
                else
                {
                    NoInvest();
                }
            }
            else
            {
                if (property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().player == 1 && !paid)
                {
                    player2.GetComponent<Money>().money += property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().lostToll;
                    player1.GetComponent<Money>().money -= property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().lostToll;
                    paid = true;
                }

                if (property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().player == 2 && !paid)
                {

                    player2.GetComponent<Money>().money += property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().returVal;
                    paid = true;
                }
                if (player2.GetComponent<FollowThePath>().jailed && !paid)
                {
                    Debug.Log("PENJARA 2");
                    player2.GetComponent<FollowThePath>().jailed = false;
                    player2.GetComponent<Money>().life -= 1;
                    paid = true;
                    player2MoveText.gameObject.SetActive(false);
                    player2bg.gameObject.SetActive(false);
                    player1MoveText.gameObject.SetActive(true);
                    player1bg.gameObject.SetActive(true);
                }
                if (player2.GetComponent<FollowThePath>().taxes && paid)
                {
                    Debug.Log("PAJAK 2");
                    player2.GetComponent<FollowThePath>().taxes = false;
                    player2.GetComponent<Money>().money -= property.propertyWaypoint[waypointTax].GetComponent<PropertyPrice>().lostToll;
                    paid = true;
                    player2MoveText.gameObject.SetActive(false);
                    player2bg.gameObject.SetActive(false);
                    player1MoveText.gameObject.SetActive(true);
                    player1bg.gameObject.SetActive(true);
                }

                if (player2.GetComponent<FollowThePath>().warehouse)
                {
                    Debug.Log("WAREHOUSE 2");
                    player2.GetComponent<FollowThePath>().warehouse = false;
                    player2.GetComponent<Money>().money += property.propertyWaypoint[waypointHouse].GetComponent<PropertyPrice>().returVal;
                    player2MoveText.gameObject.SetActive(false);
                    player2bg.gameObject.SetActive(false);
                    player1MoveText.gameObject.SetActive(true);
                    player1bg.gameObject.SetActive(true);
                }

                player2StartWaypoint = player1.GetComponent<FollowThePath>().waypointIndex;
            }
        }


        if (player1.GetComponent<Money>().money <= 0 )
        {
            player1.GetComponent<Money>().money = 2000;
            player1.GetComponent<Money>().life -= 1;

        }

        if (player2.GetComponent<Money>().money <= 0)
        {
            player2.GetComponent<Money>().money = 2000;
            player2.GetComponent<Money>().life -= 1;

        }

        if (player1.GetComponent<Money>().life == 0)
        {
            whoWinsTextShadow.gameObject.SetActive(true);
            whoWinsTextShadow.GetComponent<TextMeshProUGUI>().text = "Player 2 Wins";
            gameOver = true;
            pauseGame = true;
        }

        if (player2.GetComponent<Money>().life == 0)
        {
            whoWinsTextShadow.gameObject.SetActive(true);
            whoWinsTextShadow.GetComponent<TextMeshProUGUI>().text = "Player 1 Wins";
            gameOver = true;
        }

        
        //   
        //}

        //if (player2.GetComponent<FollowThePath>().waypointIndex >
        //   player2StartWaypoint + diceSideThrown)
        //{
        //
        //}




        //if (player1.GetComponent<FollowThePath>().waypointIndex == 
        //    player1.GetComponent<FollowThePath>().waypoints.Length)
        //{

        //    //whoWinsTextShadow.gameObject.SetActive(true);
        //    //whoWinsTextShadow.GetComponent<TextMeshProUGUI>().text = "Player 1 Wins";
        //    //gameOver = true;



        //}

        //if (player2.GetComponent<FollowThePath>().waypointIndex ==
        //    player2.GetComponent<FollowThePath>().waypoints.Length)
        //{
        //    //whoWinsTextShadow.gameObject.SetActive(true);
        //    //player1MoveText.gameObject.SetActive(false);
        //    //player2MoveText.gameObject.SetActive(false);
        //    //whoWinsTextShadow.GetComponent<TextMeshProUGUI>().text = "Player 2 Wins";
        //    //gameOver = true;

        //}

    }

    public static void MovePlayer(int playerToMove)
    {
        paid = false;
        switch (playerToMove) { 
            case 1:
                player1.GetComponent<FollowThePath>().moveAllowed = true;
                break;

            case 2:
                player2.GetComponent<FollowThePath>().moveAllowed = true;
                break;
        }
    }

    public  void InvestPopUp(int stop)
    {
        //price = priceStop;  
        price = property.propertyWaypoint[stop].GetComponent<PropertyPrice>().buyPrice;
        returnVal = property.propertyWaypoint[stop].GetComponent<PropertyPrice>().returVal;
        playerPopUp.gameObject.SetActive(true);
        priceTag.text = "$ " + price;
        returnValTag.text = "$ " + returnVal;
        //AudioScript.PlaySFX("popup");

        pauseGame = true;
    }

    public void PauseGame()
    {
        pauseGame = true;
        pausePopUp.gameObject.SetActive(true);

    }
    public void ContGame()
    {
        pauseGame = false;
        pausePopUp.gameObject.SetActive(false);
    }

    public void BackToMenu()
    {
        pauseGame = false;
        //Destroy(this);
    }
    public void YesInvest()
    {
        //AudioScript.PlaySFX("clickYes");
        pop1 = false;
        pop2 = false;
        if(dice.whosTurn == -1)
        {
            //if (player1.GetComponent<Money>().money >= property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().buyPrice)
            //{
                paid = true;
                player1.GetComponent<Money>().money -= property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().buyPrice;
                property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().bought = true;
                property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().player = 1;

                GameObject donutObject = Instantiate(donut,new Vector3 (0,0,0), donut.transform.rotation) as GameObject;
                donutObject.transform.SetParent(property.propertyWaypoint[waypointStop - diceSideThrown].transform);
                donutObject.transform.localPosition = new Vector3(0, 0, 0);

                //print(waypointStop);
                //donutObject.transform.parent = property.propertyWaypoint[waypointStop].transform;
                //Instantiate(donut, property.propertyWaypoint[waypointStop].transform.position, Quaternion.identity);
                //Debug.Log(player1.GetComponent<Money>().money);
                player1MoveText.gameObject.SetActive(false); 
                player1bg.gameObject.SetActive(false);
                player2MoveText.gameObject.SetActive(true);
                player2bg.gameObject.SetActive(true);
                Debug.Log("Invest ");

            //}
            //else
            //{
            //    print("No moneuy");
            //    NoInvest();
            //}
           
        }
        if (dice.whosTurn == 1)
        {
            //if (player2.GetComponent<Money>().money >= property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().buyPrice)
            //{
                paid = true;
                player2.GetComponent<Money>().money -= property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().buyPrice;
                property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().bought = true;
                property.propertyWaypoint[waypointStop].GetComponent<PropertyPrice>().player = 2;

                GameObject drugObject = Instantiate(drug, new Vector3(0, 0, 0), donut.transform.rotation) as GameObject;
                drugObject.transform.SetParent(property.propertyWaypoint[waypointStop].transform);
                drugObject.transform.localPosition = new Vector3(0, 0, 0);
                //drugObject.transform.SetParent(property.propertyWaypoint[waypointStop].transform);
                print(waypointStop);
                //drugObject.transform.parent = property.propertyWaypoint[waypointStop].GetComponent<Transform>();

                //Instantiate(drug, property.propertyWaypoint[waypointStop].transform.position, Quaternion.identity);
                //Debug.Log(player2.GetComponent<Money>().money);
                player1MoveText.gameObject.SetActive(true);
                player1bg.gameObject.SetActive(true);
                player2MoveText.gameObject.SetActive(false);
                player2bg.gameObject.SetActive(false);
                Debug.Log("Invest 2");

            //}
            //else
            //{
            //    print("no money 2");
            //    NoInvest();
            //}
            
        }

        pauseGame = false;
        playerPopUp.gameObject.SetActive(false);
        
        


    }

    public void NoInvest()
    {
        //AudioScript.PlaySFX("click");

        if (dice.whosTurn == -1)
        {

            pop1 = false;
            
            
            player1.GetComponent<Money>().money -= 0;
            player1MoveText.gameObject.SetActive(false);
            player2MoveText.gameObject.SetActive(true);
            
        }
        if (dice.whosTurn == 1)
        {
            pop2 = false;
            player1.GetComponent<Money>().money -= 0;
            player1MoveText.gameObject.SetActive(true);
            player2MoveText.gameObject.SetActive(false);
            
        }
       
        pauseGame = false;
        playerPopUp.gameObject.SetActive(false);

    }



}
