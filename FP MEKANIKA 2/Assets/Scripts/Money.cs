﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using System.Threading;
using TMPro;

public class Money : MonoBehaviour
{
    public int money;
    public TextMeshProUGUI moneyText;
    public TextMeshProUGUI lifetext;
    public int life;
    void Start()
    {
        money = 2000;
        life = 3;
        SetCountText();
    }


    public void SetCountText()
    {
        moneyText.text = "$ " + money.ToString();
        lifetext.text = "♥ " + life.ToString();
    }
  
    void Update()
    {
        moneyText.text = "$ " + money.ToString();
        lifetext.text = "♥ " + life.ToString();
    }

    private void OnCollisionTrigger2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("MoneyStart"))
        {
            Debug.Log("Dibayar");
            money += 300;
        }

    }
}
