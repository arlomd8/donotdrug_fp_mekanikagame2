﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Options : MonoBehaviour
{
    public AudioScript audioOpt;

    public TextMeshProUGUI sfxText;
    public TextMeshProUGUI bgmText;
    public bool sfxOn;
    public bool bgmOn;
    
    void Start()
    {
        sfxOn = true;
        bgmOn = true;
        sfxText.text = "SFX : ON";
        bgmText.text = "BGM : ON";
    }

   
    void Update()
    {
    }

    public void sfxOnControl()
    {
        if (sfxOn)
        {
            AudioScript.PlaySFX("click2");
            sfxText.text = "SFX: ON";
            sfxOn = true;

        }
        else
        {
            sfxText.text = "SFX : OFF";
            sfxOn = false;
        }
    }

    public void bgmOnControl()
    {
        if (bgmOn)
        {
            bgmText.text = "BGM : OFF";
            bgmOn = false;
            audioOpt.audio.Stop();
        }
        else
        {
            bgmText.text = "BGM : ON";
            bgmOn = true;
            audioOpt.audio.Play();
        }

        

        //if(!bgmOn)
        //{
        //    bgmText.text = "BGM : ON";
        //    bgmOn = true;
        //    audioOpt.audio.Play();
        //}

    }

}
