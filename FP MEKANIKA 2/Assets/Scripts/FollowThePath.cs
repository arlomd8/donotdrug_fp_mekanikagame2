﻿using UnityEngine;

public class FollowThePath : MonoBehaviour {

    public Transform[] waypoints;
    [SerializeField]
    private float moveSpeed = 3f;
    //[HideInInspector]
    public int waypointIndex = 0;
    public bool moveAllowed = false;

    public bool jailed = false;
    public bool taxes = false;
    public bool warehouse = false;
    public int jailpointIndex = 6;
    public int taxpointIndex = 12;
    public int warehousepointIndex = 18;
    
    private void Start () {
        transform.position = waypoints[waypointIndex].transform.position;
	}
    private void Update()
    {

        if (moveAllowed)
        { 
        Move();
        }


	}
    private void Move()
    {

        if (waypointIndex == waypoints.Length)
        {
            waypointIndex = 0;
        }

        if (waypointIndex <= waypoints.Length - 1)
        {
           
            transform.position = Vector2.MoveTowards(transform.position,
            waypoints[waypointIndex].transform.position,
            moveSpeed * Time.deltaTime);

            if (transform.position == waypoints[waypointIndex].transform.position)
            {
                // print(waypointIndex);
               
                if (GameControl.diceSideThrown == 0)
                {
                    //print("dadu habis");
                    //transform.position = waypoints[waypointIndex].transform.position;
                    waypointIndex -= 1;
                }

                GameControl.diceSideThrown -= 1;
                waypointIndex += 1;


                //print(waypointIndex);

                jailed = false;
                taxes = false;
                warehouse = false;


            }

            if(transform.position == waypoints[jailpointIndex].transform.position)
            {
                jailed = true;
            }
            if (transform.position == waypoints[taxpointIndex].transform.position)
            {
                taxes = true;
            }
            if (transform.position == waypoints[warehousepointIndex].transform.position)
            {
                warehouse = true;
            }

        }

        if (GameControl.diceSideThrown <= -1)
        {
            GameControl.diceSideThrown = 0;
            moveAllowed = false;
            
            return;

        }

        /* if(waypointIndex == 24)
         {
             Debug.Log("fay");
             {
                 if(GameControl.diceSideThrown == 1)
                 {
                     Debug.Log("error");
                 }
             }
         }
         */
    }
}
